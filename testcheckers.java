// TestCheckers CLASS:

/**
   COPYRIGHT (C) 2001 David Lee. All Rights Reserved.
   @author (David) Zeng Yuan Lee
   @version 2.0.0 April 6, 2001
*/

import Checkers;
import java.applet.Applet;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Graphics;

/**
   Loads up a (defunct) game of checkers in an applet window.
*/
public class TestCheckers extends Applet implements MouseListener
{   public void init()
    {   this.addMouseListener(this);
        gameBoard = new Checkers();
        gameBoard.initialSetUp();
        x0 = 0;
        y0 = 0;
        x1 = 0;
        y1 = 0;
    }

    public void paint(Graphics g)
    {   gameBoard.drawBoard(g);
    }

    public void mousePressed(MouseEvent event)
    {   x0 = event.getX();
        y0 = event.getY();

        x0 = gameBoard.getRow(x0);
        y0 = gameBoard.getCol(y0);
    }

    public void mouseReleased(MouseEvent event)
    {   x1 = event.getX();
        y1 = event.getY();

        x1 = gameBoard.getRow(x1);
        y1 = gameBoard.getCol(y1);

        gameBoard.board[x1 - 1][y1 - 1] = gameBoard.getPiece(x0, y0);
        gameBoard.removePiece(x0, y0);

        this.repaint();
    }

    /**
       Not suppose to do anything.
    */
    public void mouseEntered(MouseEvent event)
    {   ;
    }

    /**
       Not suppose to do anything.
    */
    public void mouseExited(MouseEvent event)
    {   ;
    }

    /**
       Not suppose to do anything.
    */
    public void mouseClicked(MouseEvent event)
    {   ;
    }

// STATES
    private Checkers gameBoard;
    private int x0;
    private int y0;
    private int x1;
    private int y1;
}
