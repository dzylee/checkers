// MODIFIED Chip CLASS:

/**
   COPYRIGHT (C) 2001 David Lee. All Rights Reserved.
   @author (David) Zeng Yuan Lee
   @version 2.0.0 April 6, 2001
*/

import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
   A subclass of Piece, used specifically for creating and manipulating the chips used in playing Checkers. Provides two methods that overrides the corresponding methods in the superclass. Class includes two constructors. Methods include copy() and drawPiece(), and all methods provided in the Piece class. All states are represented by those defined in the Piece class. There are no new states defined for this subclass.
*/
public class Chip extends Piece
{

// CONSTRUCTORS

    /**
       Creates object of type Chip with default specifications.
    */
    public Chip()
    {   super();
    }

    /**
       Creates object of type Chip with custom color, but does not set chip position.
       @param c a color object containing the color for a playing chip.
    */
    public Chip(Color c)
    {   super(c);
    }

    /**
       Creates object of type Chip with custom specifications.
       @param c a color object containing the color for a playing chip.
       @param row the row position (vertical) to place the chip on.
       @param col the column position (horizontal) to place the chip on.
    */
    public Chip(Color c, int row, int col)
    {   super(c, row, col);
    }

// OPERATIONS

    /**
       Duplicates an object of type Chip.
       @newRowPosn a new row position (vertical) for the duplicate chip.
       @newColPosn a new column position (horizontal) for the duplicate chip.
       @return parameters for an object of type Piece.
    */
    public Piece copy()
    {   return new Chip(super.getPieceColor());
    }

    /**
       Draws the playing chip on a game board using all the provided specifications.
    */
    public void drawPiece(Board b, Graphics g)
    {   Graphics2D g2 = (Graphics2D)g;

        double dx = b.xPix(2) - b.xPix(1);
        double dy = b.yPix(2) - b.yPix(1);
        double defaultChipSize;
        if (dx == dy)
        {   defaultChipSize = 0.6 * dx;
        }
        else
        {   if (dx < dy)
            {   defaultChipSize = 0.6 * dx;
            }
            else
            {   defaultChipSize = 0.6 * dy;
            }
        }

        Ellipse2D.Double chip = new Ellipse2D.Double(b.xPix(super.getColPosn()) + ((dx - defaultChipSize) / 2), b.yPix(super.getRowPosn()) + ((dx - defaultChipSize) / 2), defaultChipSize, defaultChipSize);
        g2.setColor(super.getPieceColor());
        g2.fill(chip);
    }
}