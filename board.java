// MODIFIED Board CLASS:

/**
   COPYRIGHT (C) 2001 David Lee. All Rights Reserved.
   @author (David) Zeng Yuan Lee
   @version 2.0.0 April 6, 2001
*/

import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;

/**
   Provides methods to create and manipulate a generic game board. Class includes two constructors. Methods include getWinWidth(), getWinHeight(), getNumRows(), getNumCols(), getOddColor(), getEvenColor(), windowSize(), boardSize(), oddColor(), evenColor(), squareColor(), xPix(), yPix(), getRow(), getCol() and drawBoard(). Objects of this class will have states represented by winWidth, winHeight, numRows, numCols, oddC and evenC.
*/
public class Board
{

// CONSTRUCTORS

    /**
       Creates object of type Board with default specifications.
    */
    public Board()
    {   this.winWidth = 400;
        this.winHeight = 400;
        this.numRows = 8;
        this.numCols = 8;
        this.oddC = new Color(1.0F, 1.0F, 1.0F);
        this.evenC = new Color(0.0F, 0.0F, 0.0F);
    }

    /**
       Creates object of type Board with custom specifications.
       @param w an integer width for the window size, measured in pixels.
       @param h an integer height for the windows size, measured in pixels.
       @param r the number of rows the game board will have.
       @param c the number of columns the game board wil have.
       @param o a Color object to provide the color for the odd squares on the game board.
       @param e a Color object to provide the color for the even squares on the game board.
    */
    public Board(int w, int h, int r, int c, Color o, Color e)
    {   this.winWidth = w;
        this.winHeight = h;
        this.numRows = r;
        this.numCols = c;
        this.oddC = o;
        this.evenC = e;
    }

// ACCESSORS

    /**
       Retrieves width of the window stored in a Board object.
       @return integer width of the window, measured in pixels.
    */
    public int getWinWidth()
    {   return this.winWidth;
    }

    /**
       Retrieves height of the window stored in a Board object.
       @return integer height of the window, measured in pixels.
    */
    public int getWinHeight()
    {   return this.winHeight;
    }

    /**
       Retrieves the number of rows stored in a Board object.
       @return number of rows in a Board object.
    */
    public int getNumRows()
    {   return this.numRows;
    }

    /**
       Retrieves the number of columns stored in a Board object.
       @return number of columns in a Board object.
    */
    public int getNumCols()
    {   return this.numCols;
    }

    /**
       Retrieves the color of the odd squares in a Board object.
       @return color of odd squares in a Board object.
    */
    public Color getOddColor()
    {   return this.oddC;
    }

    /**
       Retrieves the color of the even squares in a Board object.
       @return color of even squares in a Board object.
    */
    public Color getEvenColor()
    {   return this.evenC;
    }

// MUTATORS

    /**
       Requests the dimensions for a window using a modal dialog box.
    */
    public void windowSize()
    {   String userInput;
        boolean input1is = true;
        boolean input2is = true;

        while (input1is)
        {   try
            {   userInput = JOptionPane.showInputDialog("Please enter the (integer) width of the window.");
                this.winWidth = Integer.parseInt(userInput);
                input1is = false;
            }
            catch (NumberFormatException e)
            {
            }
        }

        while (input2is)
        {   try
            {   userInput = JOptionPane.showInputDialog("Please enter the (integer) height of the window.");
                this.winHeight = Integer.parseInt(userInput);
                input2is = false;
            }
            catch (NumberFormatException e)
            {
            }
        }
    }

    /**
       Requests the number of rows and columns for a game board using a modal dialog box.
    */
    public void boardSize()
    {   String userInput;
        boolean input1is = true;
        boolean input2is = true;

        while (input1is)
        {   try
            {   userInput = JOptionPane.showInputDialog("Please enter the numbers of rows on the board.");
                this.numRows = Integer.parseInt(userInput);
                input1is = false;
            }
            catch (NumberFormatException e)
            {
            }
        }

        while (input2is)
        {   try
            {   userInput = JOptionPane.showInputDialog("Please enter the number of columns on the board.");
                this.numCols = Integer.parseInt(userInput);
                input2is = false;
            }
            catch (NumberFormatException e)
            {
            }
        }
    }

    /**
       Requests the color for odd squares on a gameboard using a modal dialog box.
    */
    public void oddColor()
    {   boolean inputIs = true;

        while (inputIs)
        {   try
            {   String userInput;
                StringTokenizer tokens;

                do
                {   userInput = JOptionPane.showInputDialog("Please specify the RGB values of a color for the odd squares of a checkered board.\n(ie: '1.0, 1.0, 1.0' for white.)");
                    userInput = userInput.trim();
                    tokens = new StringTokenizer(userInput);
                }
                while (tokens.countTokens() != 3);

                String r = tokens.nextToken();
                String g = tokens.nextToken();
                String b = tokens.nextToken();

                int comma1 = r.indexOf(",");
                int comma2 = g.indexOf(",");

                r = r.substring(0, comma1);
                g = g.substring(0, comma2);

                float rFloat = Float.parseFloat(r);
                float gFloat = Float.parseFloat(g);
                float bFloat = Float.parseFloat(b);

                this.oddC = new Color(rFloat, gFloat, bFloat);
                inputIs = false;
            }
            catch (NumberFormatException e)
            {
            }
        }
    }

    /**
       Requests the color for even squares on a gameboard using a modal dialog box.
    */
    public void evenColor()
    {   boolean inputIs = true;

        while (inputIs)
        {   try
            {   String userInput;
                StringTokenizer tokens;

                do
                {   userInput = JOptionPane.showInputDialog("Please specify the RGB values of a color for the even squares of a checkered board.\n(ie: '0.0, 0.0, 0.0' for black.)");
                    userInput = userInput.trim();
                    tokens = new StringTokenizer(userInput);
                }
                while (tokens.countTokens() != 3);

                String r = tokens.nextToken();
                String g = tokens.nextToken();
                String b = tokens.nextToken();

                int comma1 = r.indexOf(",");
                int comma2 = g.indexOf(",");

                r = r.substring(0, comma1);
                g = g.substring(0, comma2);

                float rFloat = Float.parseFloat(r);
                float gFloat = Float.parseFloat(g);
                float bFloat = Float.parseFloat(b);

                this.evenC = new Color(rFloat, gFloat, bFloat);
                inputIs = false;
            }
            catch (NumberFormatException e)
            {
            }
        }
    }

// OPERATIONS

    /**
       Determines whether a square on the game board is an even or odd square. Meant to be used in conjunction with the drawBoard() method.
       @return the appropriate color depending on whether a given square is odd or even.
    */
    public Color squareColor(int r, int c)
    {   if (((r + c) % 2) == 0)
        {   return this.getEvenColor();
        }
        else
        {   return this.getOddColor();
        }
    }

    /**
       Calculates the x-pixel position for a given column on a game board.
       @return corresponding x-pixel position in the window.
    */
    public double xPix(int xCoord)
    {   return (xCoord - 1) * ((this.getWinWidth()) / this.getNumCols());
    }

    /**
       Calculates the y-pixel position for a given row on a game board.
       @return corresponding y-pixel position in the window.
    */
    public double yPix(int yCoord)
    {   return (yCoord - 1) * ((this.getWinHeight()) / this.getNumRows());
    }

    /**
       Determines the corresponding row from the x-pixel position.
       @return corresponding row on the game board.
    */
    public int getRow(int xPix)
    {   return (int)((xPix * this.getNumCols() / this.getWinWidth()) + 1);
    }

    /**
       Determines the corresponding column from the y-pixel position.
       @return corresponding column on the game board.
    */
    public int getCol(int yPix)
    {   return (int)((yPix * this.getNumRows() / this.getWinHeight()) + 1);
    }

    /**
       Draws the game board in window using all the provided specifications.
    */
    public void drawBoard(Graphics g)
    {   Graphics2D g2 = (Graphics2D)g;

        Rectangle2D.Double winDimensions = new Rectangle2D.Double(0, 0, this.getWinWidth() - 1, this.getWinHeight() - 1);
        g2.setColor(Color.white);
        g2.fill(winDimensions);

        for (int y = 1; y <= this.getNumRows() + 1; y++)
        {   for (int x = 1; x <= this.getNumCols() + 1; x++)
            {   Rectangle2D.Double square = new Rectangle2D.Double(this.xPix(x), this.yPix(y), this.xPix(x + 1), this.yPix(y + 1));
                g2.setColor(this.squareColor(x, y));
                g2.fill(square);
            }
        }

        g2.setColor(Color.black);
        for (int x = 0; x <= this.getNumCols() + 1; x++)
        {   Line2D.Double verLines = new Line2D.Double(this.xPix(x), 0, this.xPix(x), this.getWinHeight());
            g2.draw(verLines);
        }

        for (int y = 0; y <= this.getNumRows() + 1; y++)
        {   Line2D.Double horLines = new Line2D.Double(0, this.yPix(y), this.getWinWidth(), this.yPix(y));
            g2.draw(horLines);
        }
    }

// STATES

    private int winWidth;
    private int winHeight;
    private int numRows;
    private int numCols;
    private Color oddC;
    private Color evenC;
}