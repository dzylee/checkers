// MODIFIED Piece CLASS:

/**
   COPYRIGHT (C) 2001 David Lee. All Rights Reserved.
   @author (David) Zeng Yuan Lee
   @version 2.0.0 April 6, 2001
*/

import java.awt.Color;
import java.awt.Font;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;

/**
   Provides methods to create and manipulate a generic game board. Class includes three constructors. Methods include getPieceColor(), getRowPosn(), getColPosn(), pieceColor(), rowPosn(), colPosn(), copy() and drawPiece(). Objects of this class will have states represented by pieceColor, rowPosn, colPosn.
*/
public class Piece
{

// CONSTRUCTORS

    /**
       Creates object of type Piece with default specifications.
    */
    public Piece()
    {   this.pieceColor = new Color(1.0F, 0.0F, 0.0F);
        this.rowPosn = 1;
        this.colPosn = 1;
    }

    /**
       Creates object of type Piece with custom color, but does not set piece position.
       @param c a Color object containing the color for a playing piece.
    */
    public Piece(Color c)
    {   this.pieceColor = c;
    }

    /**
       Creates object of type Piece with custom specifications.
       @param c a Color object containing the color for a playing piece.
       @param row the row position (vertical) to place the piece on.
       @param col the column position (horizontal) to place the piece on.
    */
    public Piece(Color c, int row, int col)
    {   this.pieceColor = c;
        this.rowPosn = row;
        this.colPosn = col;
    }

// ACCESSORS

    /**
       Retrieves the color of a playing piece.
       @return color of a playing piece.
    */
    public Color getPieceColor()
    {   return this.pieceColor;
    }

    /**
       Retrieves the row position (vertical) of a playing piece.
       @return row position (vertical) of a playing piece.
    */
    public int getRowPosn()
    {   return this.rowPosn;
    }

    /**
       Retrieves the column position (horizontal) of a playing piece.
       @return column position (horizontal) of a playing piece.
    */
    public int getColPosn()
    {   return this.colPosn;
    }

// MUTATORS

    /**
       Requests a custom color for a playing piece by using a modal dialog box.
    */
    public void pieceColor()
    {   boolean inputIs = true;

        while (inputIs)
        {   try
            {   String userInput;
                StringTokenizer tokens;

                do
                {   userInput = JOptionPane.showInputDialog("Please specify the RGB values of a color for a playing piece.\n(ie: 1.0, 0.0, 0.0)");
                    userInput = userInput.trim();
                    tokens = new StringTokenizer(userInput);
                }
                while (tokens.countTokens() != 3);

                String r = tokens.nextToken();
                String g = tokens.nextToken();
                String b = tokens.nextToken();

                int comma1 = r.indexOf(",");
                int comma2 = g.indexOf(",");

                r = r.substring(0, comma1);
                g = g.substring(0, comma2);

                float rFloat = Float.parseFloat(r);
                float gFloat = Float.parseFloat(g);
                float bFloat = Float.parseFloat(b);

                this.pieceColor = new Color(rFloat, gFloat, bFloat);
                inputIs = false;
            }
            catch (NumberFormatException e)
            {
            }
        }
    }

    /**
       Requests a row position (vertical) for a playing piece by using a modal dialog box.
    */
    public void rowPosn(int r)
    {   this.rowPosn = r;
    }

    /**
       Requests a column position (horizontal) for a playing piece by using a modal dialog box.
    */
    public void colPosn(int c)
    {   this.colPosn = c;
    }

// OPERATIONS

    /**
       Duplicates an object of type Piece.
       @return parameters for an object of type Piece.
    */
    public Piece copy()
    {   return new Piece(this.getPieceColor());
    }

    /**
       Draws the playing piece on a game board using all the provided specifications.
    */
    public void drawPiece(Board b, Graphics g)
    {   b.drawBoard(g);
        Graphics2D g2 = (Graphics2D)g;

        FontRenderContext defaultContext = g2.getFontRenderContext();

        double dx = b.xPix(2) - b.xPix(1);
        double dy = b.yPix(2) - b.yPix(1);
        int defaultFontSize;
        if (dx == dy)
        {   defaultFontSize = (int)(0.8 * dx);
        }
        else
        {   if (dx < dy)
            {   defaultFontSize = (int)(0.8 * dx);
            }
            else
            {   defaultFontSize = (int)(0.8 * dy);
            }
        }

        Font defaultFont = new Font("SansSerif", Font.BOLD, defaultFontSize);
        TextLayout defaultLayout = new TextLayout("*", defaultFont, defaultContext);

        dx = ((dx - defaultLayout.getAdvance()) / 2) + b.xPix(this.colPosn);
        dy = ((dy - (defaultLayout.getAscent() + defaultLayout.getDescent())) / 2) + b.yPix(this.rowPosn + 1);

        float xFloat = (float)(dx);
        float yFloat = (float)(dy);

        g2.setFont(defaultFont);
        g2.setColor(this.pieceColor);
        g2.drawString("*", xFloat, yFloat);
    }

// STATES

    private Color pieceColor;
    private int rowPosn;
    private int colPosn;

}