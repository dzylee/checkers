// MODIFIED Checkers CLASS:

/**
   COPYRIGHT (C) 2001 David Lee. All Rights Reserved.
   @author (David) Zeng Yuan Lee
   @version 2.0.0 April 6, 2001
*/

import Chip;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

/**
   A subclass of Board, used specifically for creating and manipulating custom game boards. Provides one method that overrides the corresponding method in the superclass. Class includes one constructor. Methods include getHomeColor(), getAwayColor() getPiece(), removePiece(), placePiece(), isOccupied(), initialSetup() and drawBoard(), and all methods provided in the Board class. All states are represented by those defined in the Board class. There are three new states defined: homeColor, awayColor and board.
*/
public class Checkers extends Board
{

// CONSTRUCTORS

    /**
       Creates object of type Chip with default specifications specified in superclass and initializes array, board, with null for all values.
    */
    public Checkers()
    {   super();
        board = new Chip[8][8];
        for (int row = 0; row <= 7; row++)
        {   for (int col = 0; col <= 7; col++)
            {   board[row][col] = null;
            }
        }
    }

// ACCESSORS

    /**
       Retrieves the color of the playing pieces for the home player.
       @return the color of the playing pieces for the home player.
    */
    public Color getHomeColor()
    {   return homeColor;
    }

    /**
       Retrieves the color of the playing pieces for the away player.
       @return the color of the playing pieces for the away player.
    */
    public Color getAwayColor()
    {   return awayColor;
    }

    /**
       Retrieves the current playing chip on a specified square.
       @param r the row position on the checkers board.
       @param c the column position on the checkers board.
       @return the chip on the specified square.
    */
    public Chip getPiece(int r, int c)
    {   return this.board[r - 1][c - 1];
    }

    /**
       Removes the current playing chip on a specified square.
       @param r the row position on the checkers board.
       @param c the column position on the checkers board.
    */
    public void removePiece(int r, int c)
    {   this.board[r - 1][c - 1] = null;
    }

    /**
       Places the playing chip at a board position specified by the piece's position.
       @param p the piece (or chip) object to be placed on the board.
    */
    public void placePiece(Piece p)
    {   // !!! this method is wrong somehow.
        this.board[p.getRowPosn() - 1][p.getColPosn() - 1] = (Chip)p;
    }

    /**
       Determines whether a board position is occupied by a playing chip.
       @param r the row position on the checkers board.
       @param c the column position on the checkers board.
       @return true if the square position is occupied; false otherwise.
    */
    public boolean isOccupied(int r, int c)
    {   if (this.board[r - 1][c - 1] == null)
        {   return false;
        }
        else
        {   return true;
        }
    }

    /**
       Sets all the playing chips into their correct starting position in the array, board.
    */
    public void initialSetUp()
    {   for (int row = 0; row <= 2; row++)
        {   for (int col = 0; col <= 7; col++)
            {   if (((row + col) % 2) == 0)
                {   this.board[row][col] = new Chip(this.getAwayColor(), row + 1, col + 1);
                }
                else
                {   this.board[row][col] = null;
                }
            }
        }

        for (int row = 5; row <= 7; row++)
        {   for (int col = 0; col <= 7; col++)
            {   if (((row + col) % 2) == 0)
                {   this.board[row][col] = new Chip(this.getHomeColor(), row + 1 , col + 1);
                }
                else
                {   this.board[row][col] = null;
                }
            }
        }
    }

// OPERATIONS

    /**
       Draws the Checkers board and all the playing pieces in their starting layout in window using all the provided specifications.
    */
    public void drawBoard(Graphics g)
    {   Graphics2D g2 = (Graphics2D)g;

        g2.setColor(Color.white);
        g2.fillRect(0, 0, super.getWinWidth() - 1, super.getWinHeight() - 1);

        for (int y = 1; y <= super.getNumRows() + 1; y++)
        {   for (int x = 1; x <= super.getNumCols() + 1; x++)
            {   Rectangle2D.Double square = new Rectangle2D.Double(super.xPix(x), super.yPix(y), super.xPix(x + 1), super.yPix(y + 1));
                g2.setColor(super.squareColor(x, y));
                g2.fill(square);
            }
        }

        g2.setColor(Color.black);
        for (int x = 1; x <= super.getNumCols() + 1; x++)
        {   Line2D.Double verLines = new Line2D.Double(super.xPix(x), 0, super.xPix(x), super.getWinHeight());
            g2.draw(verLines);
        }

        for (int y = 1; y <= super.getNumRows() + 1; y++)
        {   Line2D.Double horLines = new Line2D.Double(0, super.yPix(y), super.getWinWidth(), super.yPix(y));
            g2.draw(horLines);
        }

        for (int row = 0; row <= 7; row++)
        {   for (int col = 0; col <= 7; col++)
            {   if (this.board[row][col] == null)
                {   ;
                }
                else
                {   this.board[row][col].drawPiece(this, g);
                }
            }
        }
    }

// STATES

    private static final Color homeColor = new Color(1.0F, 0.0F, 0.0F);
    private static final Color awayColor = new Color(0.0F, 0.0F, 1.0F);
    public static Chip[][] board;
}